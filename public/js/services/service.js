(function () {
    angular.module("singboapp")
        .service("dbService", dbService);

    dbService.$inject = ["$http", "$q"];

    function dbService($http, $q) {
        var service = this;

        service.getTotalDefaultApps = function (callback){
            var defer = $q.defer();
            $http.get("/api/defaultapp/totalcnt/")
                .then(function(result){
                    defer.resolve(result);
                }).catch(function(error){
                    defer.reject(error);
                });
            return defer.promise;
        };

        service.defaultAppPaging = function (offset, recPerpage, callback){
            var defer = $q.defer();
            $http.get("/api/defaultapp/pagination/" + offset + "/" + recPerpage)
                .then(function(results){
                    defer.resolve(results);
                }).catch(function(error){
                    defer.reject(error);
                });
            return defer.promise;
        };


        service.getTotalCustomApps = function (callback){
            var defer = $q.defer();
            $http.get("/api/customapp/totalcnt/")
                .then(function(result){
                    defer.resolve(result);
                }).catch(function(error){
                    defer.reject(error);
                });
            return defer.promise;
        };

        service.customAppPaging = function (offset, recPerpage, callback){
            var defer = $q.defer();
            $http.get("/api/customapp/pagination/" + offset + "/" + recPerpage)
                .then(function(results){
                    defer.resolve(results);
                }).catch(function(error){
                    defer.reject(error);
                });
            return defer.promise;
        };

        service.getLocalProfile = function (callback){
            var defer = $q.defer();
            $http.get("/api/user/profile/")
                .then(function(result){
                    defer.resolve(result);
                }).catch(function(error){
                    defer.reject(error);
                });
            return defer.promise;
        };
        
        service.getAllSocialLoginsProfile = function (callback){
            var defer = $q.defer();
            $http.get("/api/user/social/profiles")
                .then(function(result){
                    defer.resolve(result);
                }).catch(function(error){
                defer.reject(error);
            });
            return defer.promise;
        };
    }
})();
