
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('webhooks', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    application_id: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    app_id: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    event: {
      type: DataTypes.STRING,
      allowNull: true
    },
    callback_url: {
      type: DataTypes.STRING,
      allowNull: true
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false
    }
  }, {
    tableName: 'webhooks'
  });
};
